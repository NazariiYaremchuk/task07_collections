package com.model;

public interface Model<K extends Comparable<K>, V> {
    int size();

    boolean isEmpty = false;

    V get(Object key);

    V remove(Object key);
}
