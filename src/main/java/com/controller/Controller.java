package com.controller;

import com.model.CustomTreeMap;

public class Controller {

    CustomTreeMap<String, String> customTreeMap = new CustomTreeMap();

    public String print() {
        String answer = "";
        for (CustomTreeMap.Entry<String, String> entry : customTreeMap.entrySet()) {
            answer += ("\nKey: " + entry.getKey() + "\t Value: " + entry.getValue());
        }
        return answer;
    }

    public String put(String key, String value) {
        return customTreeMap.put(key, value);
    }

    public String get(String key) {
        return customTreeMap.get(key);
    }


    public String remove(String key) {
        return customTreeMap.remove(key);
    }

}
